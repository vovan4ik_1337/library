﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class AuthorService : IAuthorService
    {
        private IUnitOfWork _unitOfWork;

        public AuthorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddAuthor(AuthorDto authorDto)
        {
            var author = new Author
            {
                AuthorLastName = authorDto?.AuthorLastName,
                AuthorFirstName = authorDto?.AuthorFirstName,
                AuthorMiddleName = authorDto?.AuthorMiddleName,
            };

            await _unitOfWork.AuthorRepository.AddAuthor(author);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteAuthor(int authorId)
        {
            await _unitOfWork.AuthorRepository.DeleteAuthor(authorId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<AuthorDto> GetAuthor(int authorId)
        {
            var author = await _unitOfWork.AuthorRepository.GetAuthor(authorId);

            if (author != null)
            {
                var authorDto = new AuthorDto
                {
                    AuthorId = author.AuthorId,
                    AuthorLastName = author?.AuthorLastName,
                    AuthorFirstName = author?.AuthorFirstName,
                    AuthorMiddleName = author?.AuthorMiddleName
                };

                return authorDto;
            }
            else
                throw new Exception("Ошибка при получении автора с id = " + authorId);
        }

        public async Task<IEnumerable<AuthorDto>> GetAuthors(string authorLastName, string authorFirstName, string authorMiddleName, int? start, int? end)
        {
            var authors = await _unitOfWork.AuthorRepository.GetAuthors(authorLastName, authorFirstName, authorMiddleName, start, end);

            var authorsDtos = authors.Select(x => new AuthorDto { 
                AuthorId = x.AuthorId,
                AuthorLastName = x?.AuthorLastName,
                AuthorFirstName = x?.AuthorFirstName,
                AuthorMiddleName = x?.AuthorMiddleName
            });

            return authorsDtos;
        }

        public async Task UpdateAuthor(int authorId, AuthorDto authorDto)
        {
            var author = new Author
            {
                AuthorLastName = authorDto?.AuthorLastName,
                AuthorFirstName = authorDto?.AuthorFirstName,
                AuthorMiddleName = authorDto?.AuthorMiddleName,
            };

            await _unitOfWork.AuthorRepository.UpdateAuthor(authorId, author);
            await _unitOfWork.CommitAsync();
        }
    }
}
