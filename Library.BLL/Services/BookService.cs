﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;

        public BookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddBook(BookDto bookDto)
        {
            var book = new Book()
            { 
                BookArticleNumber = bookDto?.BookArticleNumber,
                BookName = bookDto?.BookName,
                BookPublishingHouse = bookDto?.BookPublishingHouse,
                BookSeriesName = bookDto?.BookSeriesName,
                BookPublishingYear = bookDto?.BookPublishingYear,
                BookPrice = bookDto?.BookPrice,
                BookAgeLimit = bookDto?.BookAgeLimit,
                AuthorId = bookDto?.AuthorId,
                BookLanguageId = bookDto?.BookLanguageId
            };

            await _unitOfWork.BookRepository.AddBook(book);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteBook(int bookId)
        {
            await _unitOfWork.BookRepository.DeleteBook(bookId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<BookDto> GetBook(int bookId)
        {
            var book = await _unitOfWork.BookRepository.GetBook(bookId);

            if (book != null)
            {
                var bookDto = new BookDto
                {
                    BookId = book.BookId,
                    BookArticleNumber = book?.BookArticleNumber,
                    BookName = book?.BookName,
                    BookPublishingHouse = book?.BookPublishingHouse,
                    BookSeriesName = book?.BookSeriesName,
                    BookPublishingYear = book?.BookPublishingYear,
                    BookPrice = book?.BookPrice,
                    BookAgeLimit = book?.BookAgeLimit,
                    Author = book?.Author?.FullName,
                    AuthorId = book?.AuthorId,
                    BookLanguage = book?.BookLanguage?.BookLanguageName,
                    BookLanguageId = book?.BookLanguageId
                };

                return bookDto;
            }
            else
                throw new Exception("Ошибка при получении книги с id = " + bookId);
        }

        public async Task<IEnumerable<BookDto>> GetBooks(string bookArticleNumber, string bookName, string bookPublishingHouse, 
                                                         string bookSeriesName, string bookPublishingYear, int? bookAgeLimit,
                                                         int? authorId, int? bookLanguageId, int? genreId, int? start, int? end)
        {
            var books = await _unitOfWork.BookRepository.GetBooks(bookArticleNumber, bookName, bookPublishingHouse,
                                                                    bookSeriesName, bookPublishingYear, bookAgeLimit,
                                                                     authorId, bookLanguageId, genreId, start, end);

            var booksDtos = books.Select(x => new BookDto 
            {
                BookId = x.BookId,
                BookArticleNumber = x?.BookArticleNumber,
                BookName = x?.BookName,
                BookPublishingHouse = x?.BookPublishingHouse,
                BookSeriesName = x?.BookSeriesName,
                BookPublishingYear = x?.BookPublishingYear,
                BookPrice = x?.BookPrice,
                BookAgeLimit = x?.BookAgeLimit,
                Author = x?.Author?.FullName,
                AuthorId = x?.AuthorId,
                BookLanguage = x?.BookLanguage?.BookLanguageName,
                BookLanguageId = x?.BookLanguageId
            });

            return booksDtos;
        }

        public async Task UpdateBook(int bookId, BookDto bookDto)
        {
            var book = new Book()
            {
                BookArticleNumber = bookDto?.BookArticleNumber,
                BookName = bookDto?.BookName,
                BookPublishingHouse = bookDto?.BookPublishingHouse,
                BookSeriesName = bookDto?.BookSeriesName,
                BookPublishingYear = bookDto?.BookPublishingYear,
                BookPrice = bookDto?.BookPrice,
                BookAgeLimit = bookDto?.BookAgeLimit,
                AuthorId = bookDto?.AuthorId,
                BookLanguageId = bookDto?.BookLanguageId
            };

            await _unitOfWork.BookRepository.UpdateBook(bookId, book);
            await _unitOfWork.CommitAsync();
        }
    }
}
