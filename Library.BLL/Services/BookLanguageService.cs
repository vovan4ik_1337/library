﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class BookLanguageService : IBookLanguageService
    {
        private readonly IUnitOfWork _unitOfWork;

        public BookLanguageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddBookLanguage(BookLanguageDto bookLanguageDto)
        {
            var bookLanguage = new BookLanguage 
            {
                BookLanguageName = bookLanguageDto?.BookLanguageName
            };

            await _unitOfWork.BookLanguageRepository.AddBookLanguage(bookLanguage);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteBookLanguage(int bookLanguageId)
        {
            await _unitOfWork.BookLanguageRepository.DeleteBookLanguage(bookLanguageId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<BookLanguageDto> GetBookLanguage(int bookLanguageId)
        {
            var bookLanguage = await _unitOfWork.BookLanguageRepository.GetBookLanguage(bookLanguageId);

            if (bookLanguage != null)
            {
                var bookLanguageDto = new BookLanguageDto
                { 
                    BookLanguageId = bookLanguage.BookLanguageId,
                    BookLanguageName = bookLanguage?.BookLanguageName
                };


                return bookLanguageDto;
            }
            throw new Exception("Ошибка при получении языка с id = " + bookLanguageId);
        }

        public async Task<IEnumerable<BookLanguageDto>> GetBookLanguages()
        {
            var bookLanguages = await _unitOfWork.BookLanguageRepository.GetBookLanguages();

            var bookLanguagesDtos = bookLanguages.Select(x => new BookLanguageDto 
            {
                BookLanguageId = x.BookLanguageId,
                BookLanguageName = x?.BookLanguageName
            });

            return bookLanguagesDtos;
        }

        public async Task UpdateBookLanguage(int bookLanguageId, BookLanguageDto bookLanguageDto)
        {
            var bookLanguage = new BookLanguage
            {
                BookLanguageName = bookLanguageDto?.BookLanguageName
            };

            await _unitOfWork.BookLanguageRepository.UpdateBookLanguage(bookLanguageId, bookLanguage);
            await _unitOfWork.CommitAsync();
        }
    }
}
