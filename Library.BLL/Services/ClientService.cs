﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class ClientService : IClientService
    {
        private readonly IUnitOfWork _unitOfWork;


        public ClientService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddClient(ClientDto clientDto)
        {
            var client = new Client 
            {
                ClientLastName = clientDto?.ClientLastName,
                ClientFirstName = clientDto?.ClientFirstName,
                ClientMiddleName = clientDto?.ClientMiddleName,
                Age = clientDto?.Age,
                PhoneNumber = clientDto?.PhoneNumber              
            };

            await _unitOfWork.ClientRepository.AddClient(client);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteClient(int clientId)
        {
            await _unitOfWork.ClientRepository.DeleteClient(clientId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<ClientDto> GetClient(int clientId)
        {
            var client = await _unitOfWork.ClientRepository.GetClient(clientId);

            if (client != null)
            {
                var clientDto = new ClientDto
                { 
                    ClientId = client.ClientId,
                    ClientLastName = client?.ClientLastName,
                    ClientFirstName = client?.ClientFirstName,
                    ClientMiddleName = client?.ClientMiddleName,
                    Age = client?.Age,
                    PhoneNumber = client?.PhoneNumber
                };

                return clientDto;
            }
            else
            {
                throw new Exception("Ошибка при получении читателя с id = " + clientId);
            }
        }

        public async Task<IEnumerable<ClientDto>> GetClients(string clientLastName, string clientFirstName, string clientMiddleNamer)
        {
            var clients = await _unitOfWork.ClientRepository.GetClients(clientLastName, clientFirstName, clientMiddleNamer);

            var clientsDtos = clients.Select(x => new ClientDto 
            {
                ClientId = x.ClientId,
                ClientLastName = x?.ClientLastName,
                ClientFirstName = x?.ClientFirstName,
                ClientMiddleName = x?.ClientMiddleName,
                Age = x?.Age,
                PhoneNumber = x?.PhoneNumber
            });

            return clientsDtos;
        }

        public async Task UpdateClient(int clientId, ClientDto clientDto)
        {
            var client = new Client
            {
                ClientLastName = clientDto?.ClientLastName,
                ClientFirstName = clientDto?.ClientFirstName,
                ClientMiddleName = clientDto?.ClientMiddleName,
                Age = clientDto?.Age,
                PhoneNumber = clientDto?.PhoneNumber
            };

            await _unitOfWork.ClientRepository.UpdateClient(clientId, client);
            await _unitOfWork.CommitAsync();
        }
    }
}
