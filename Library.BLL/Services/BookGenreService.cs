﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    class BookGenreService : IBookGenreService
    {
        private IUnitOfWork _unitOfWork;

        public BookGenreService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddBookGenre(BookGenreDto bookGenreDto)
        {
            var bookGenre = new BookGenre
            { 
                BookId = bookGenreDto?.BookId,
                GenreId = bookGenreDto?.GenreId
            };

            await _unitOfWork.BookGenreRepository.AddBookGenre(bookGenre);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteBookGenre(int bookGenreId)
        {
            await _unitOfWork.BookGenreRepository.DeleteBookGenre(bookGenreId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<BookGenreDto> GetBookGenre(int bookGenreId)
        {
            var bookGenre = await _unitOfWork.BookGenreRepository.GetBookGenre(bookGenreId);

            if (bookGenre != null)
            {
                var bookGenreDto = new BookGenreDto
                {
                    BookGenreId = bookGenre.BookGenreId,
                    BookId = bookGenre?.BookId,
                    Book = bookGenre?.Book?.BookName,
                    GenreId = bookGenre?.GenreId,
                    Genre = bookGenre?.Genre?.GenreName
                };

                return bookGenreDto;
            }
            else
                throw new Exception("Ошибка при получении записи BookGenre с id = " + bookGenreId);
        }

        public async Task<IEnumerable<BookGenreDto>> GetBookGenres(int? bookId, int? genreId)
        {
            var bookGenres = await _unitOfWork.BookGenreRepository.GetBookGenres(bookId, genreId);

            var booksGenresDtos = bookGenres.Select(x => new BookGenreDto
            {
                BookGenreId = x.BookGenreId,
                BookId = x?.BookId,
                Book = x?.Book?.BookName,
                GenreId = x?.GenreId,
                Genre = x?.Genre?.GenreName
            });

            return booksGenresDtos;
        }

        public async Task UpdateGenre(int bookGenreId, BookGenreDto bookGenreDto)
        {
            var bookGenre = new BookGenre
            {
                BookId = bookGenreDto?.BookId,
                GenreId = bookGenreDto?.GenreId
            };

            await _unitOfWork.BookGenreRepository.UpdateGenre(bookGenreId ,bookGenre);
            await _unitOfWork.CommitAsync();
        }
    }
}
