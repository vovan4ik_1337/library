﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class GenreService : IGenreService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GenreService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddGenre(GenreDto genreDto)
        {
            var genre = new Genre 
            {
                GenreName = genreDto?.GenreName
            };

            await _unitOfWork.GenreRepository.AddGenre(genre);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteGenre(int genreId)
        {
            await _unitOfWork.GenreRepository.DeleteGenre(genreId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<GenreDto> GetGenre(int genreId)
        {
            var genre = await _unitOfWork.GenreRepository.GetGenre(genreId);

            if (genre != null)
            {
                var genreDto = new GenreDto 
                {
                    GenreId = genre.GenreId,
                    GenreName = genre?.GenreName
                };

                return genreDto;
            }
            else
            {
                throw new Exception("Ошибка при получении жанра с id = " + genreId);
            }
        }

        public async Task<IEnumerable<GenreDto>> GetGenres()
        {
            var genres = await _unitOfWork.GenreRepository.GetGenres();

            var genresDto = genres.Select(x => new GenreDto
            {
                GenreId = x.GenreId,
                GenreName = x?.GenreName
            });

            return genresDto;
        }

        public async Task UpdateGenre(int genreId, GenreDto genreDto)
        {
            var genre = new Genre
            {
                GenreName = genreDto?.GenreName
            };

            await _unitOfWork.GenreRepository.UpdateGenre(genreId, genre);
            await _unitOfWork.CommitAsync();
        }
    }
}
