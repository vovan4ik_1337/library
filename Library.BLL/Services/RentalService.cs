﻿using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    public class RentalService : IRentalService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RentalService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddRental(RentalDto rentalDto)
        {
            var rental = new Rental
            { 
                StartDate = rentalDto?.StartDate,
                EndDate = rentalDto?.EndDate,
                BookId = rentalDto?.BookId,
                ClientId = rentalDto?.ClientId
            };

            await _unitOfWork.RentalRepository.AddRental(rental);
            await _unitOfWork.CommitAsync();
        }

        public async Task DeleteRental(int rentalId)
        {
            await _unitOfWork.RentalRepository.DeleteRental(rentalId);
            await _unitOfWork.CommitAsync();
        }

        public async Task<RentalDto> GetRental(int rentalId)
        {
            var rental = await _unitOfWork.RentalRepository.GetRental(rentalId);

            if (rental != null)
            {
                var rentalDto = new RentalDto
                {
                    RentalId = rental.RentalId,
                    StartDate = rental?.StartDate,
                    EndDate = rental?.EndDate,
                    Book = rental?.Book?.BookName,
                    BookId = rental?.BookId,
                    Client = rental?.Client?.FullName,
                    ClientId = rental?.ClientId
                };

                return rentalDto;
            }
            else
            {
                throw new Exception("Ошибка при получении аренды с id = " + rentalId);
            }
        }

        public async Task<IEnumerable<RentalDto>> GetRentals(DateTime? startDate, DateTime? endDate, int? bookId, int? clientId)
        {
            var rentals = await _unitOfWork.RentalRepository.GetRentals(startDate, endDate, bookId, clientId);

            var rentalsDtos = rentals.Select(x => new RentalDto
            {
                RentalId = x.RentalId,
                StartDate = x?.StartDate,
                EndDate = x?.EndDate,
                Book = x?.Book?.BookName,
                BookId = x?.BookId,
                Client = x?.Client?.FullName,
                ClientId = x?.ClientId
            });

            return rentalsDtos;
        }

        public async Task UpdateRental(int rentalId, RentalDto rentalDto)
        {
            var rental = new Rental
            {
                StartDate = rentalDto?.StartDate,
                EndDate = rentalDto?.EndDate,
                BookId = rentalDto?.BookId,
                ClientId = rentalDto?.ClientId
            };

            await _unitOfWork.RentalRepository.UpdateRental(rentalId, rental);
            await _unitOfWork.CommitAsync();
        }
    }
}
