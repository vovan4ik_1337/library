﻿using Library.BLL.Interfaces;
using Library.BLL.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BLL
{
    public static class ServiceRegistrations
    {
        public static void RegisterBLL(this IServiceCollection services)
        {
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IBookLanguageService, BookLanguageService>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IGenreService, GenreService>();
            services.AddScoped<IRentalService, RentalService>();
            services.AddScoped<IBookGenreService, BookGenreService>();
        }
    }
}
