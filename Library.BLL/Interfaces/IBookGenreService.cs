﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IBookGenreService
    {
        Task<BookGenreDto> GetBookGenre(int bookGenreId);

        Task<IEnumerable<BookGenreDto>> GetBookGenres(int? bookId, int? genreId);

        Task AddBookGenre(BookGenreDto bookGenreDto);

        Task UpdateGenre(int bookGenreId, BookGenreDto bookGenreDto);

        Task DeleteBookGenre(int bookGenreId);
    }
}
