﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IAuthorService
    {
        Task<AuthorDto> GetAuthor(int authorId);

        Task<IEnumerable<AuthorDto>> GetAuthors(string authorLastName, string authorFirstName, string authorMiddleName,
            int? start, int? end);

        Task AddAuthor(AuthorDto authorDto);

        Task UpdateAuthor(int authorId, AuthorDto authorDto);

        Task DeleteAuthor(int authorId);
    }
}
