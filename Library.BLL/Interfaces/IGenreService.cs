﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IGenreService
    {
        Task<GenreDto> GetGenre(int genreId);

        Task<IEnumerable<GenreDto>> GetGenres();

        Task AddGenre(GenreDto genreDto);

        Task UpdateGenre(int genreId, GenreDto genreDto);

        Task DeleteGenre(int genreId);
    }
}
