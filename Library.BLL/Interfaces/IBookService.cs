﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IBookService
    {
        Task<BookDto> GetBook(int bookId);

        Task<IEnumerable<BookDto>> GetBooks(string bookArticleNumber, string bookName, string bookPublishingHouse,
                                            string bookSeriesName, string bookPublishingYear, int? bookAgeLimit,
                                            int? authorId, int? bookLanguageId, int? genreId, int? start, int? end);

        Task AddBook(BookDto bookDto);

        Task UpdateBook(int bookId, BookDto bookDto);

        Task DeleteBook(int bookId);
    }
}
