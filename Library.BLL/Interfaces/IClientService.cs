﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IClientService
    {
        Task<ClientDto> GetClient(int clientId);

        Task<IEnumerable<ClientDto>> GetClients(string clientLastName, string clientFirstName, string clientMiddleNamer);

        Task AddClient(ClientDto clientDto);

        Task UpdateClient(int clientId, ClientDto clientDto);

        Task DeleteClient(int clientId);
    }
}
