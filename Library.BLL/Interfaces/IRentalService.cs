﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IRentalService
    {
        Task<RentalDto> GetRental(int rentalId);

        Task<IEnumerable<RentalDto>> GetRentals(DateTime? startDate, DateTime? endDate, int? bookId, int? clientId);

        Task AddRental(RentalDto rentalDto);

        Task UpdateRental(int rentalId, RentalDto rentalDto);

        Task DeleteRental(int rentalId);
    }
}
