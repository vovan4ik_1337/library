﻿using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface IBookLanguageService
    {
        Task<BookLanguageDto> GetBookLanguage(int bookLanguageId);

        Task<IEnumerable<BookLanguageDto>> GetBookLanguages();

        Task AddBookLanguage(BookLanguageDto bookLanguageDto);

        Task UpdateBookLanguage(int bookLanguageId, BookLanguageDto bookLanguageDto);

        Task DeleteBookLanguage(int bookLanguageId);
    }
}
