﻿using Library.DomainModel.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.DomainModel
{
    public static class ServiceRegistrations
    {
        public static void RegisterDomainModel(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddDbContextPool<LibraryContext>(
            options => options.UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]));
        }
        
    }
        
}
