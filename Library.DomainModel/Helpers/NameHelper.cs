﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.DomainModel.Helpers
{
    public static class NameHelper
    {
        public static string GetPersonFullName(string lastName, string firstName, string middleName)
        {
            if (!string.IsNullOrEmpty(middleName))
                return $"{lastName} {firstName} {middleName}";

            return $"{lastName} {firstName}";
        }

        public static string GetPersonShortName(string lastName, string firstName, string middleName)
        {
            if (!string.IsNullOrEmpty(middleName))
                return $"{lastName} {firstName[0]}. {middleName[0]}.";

            return $"{lastName} {firstName[0]}.";
        }
    }
}
