﻿using Library.DomainModel.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }
        public string ClientLastName { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientMiddleName { get; set; }
        public int? Age { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Rental> Rentals { get; set; }

        public string FullName => NameHelper.GetPersonFullName(ClientLastName, ClientFirstName, ClientMiddleName);
        public string ShortName => NameHelper.GetPersonShortName(ClientLastName, ClientFirstName, ClientMiddleName);
    }
}
