﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class BookGenre
    {
        [Key]
        public int BookGenreId { get; set; }

        public int? BookId { get; set; }
        public virtual Book Book { get; set; }

        public int? GenreId { get; set; }
        public virtual Genre Genre { get; set; }

        public bool IsDeleted { get; set; }
    }
}
