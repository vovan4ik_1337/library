﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Library.DomainModel.Models
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }
        public string BookArticleNumber { get; set; }
        public string BookName { get; set; }
        public string BookPublishingHouse { get; set; }
        public string BookSeriesName { get; set; }
        public string BookPublishingYear { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal? BookPrice { get; set; }
        public int? BookAgeLimit { get; set; }
        public bool IsDeleted { get; set; }

        public int? AuthorId { get; set; }
        public virtual Author Author { get; set; }

        public int? BookLanguageId { get; set; }
        public virtual BookLanguage BookLanguage { get; set; }

        public virtual ICollection<BookGenre> BookGenres { get; set; }

        public virtual ICollection<Rental> Rentals { get; set; }
    }
}
