﻿using Library.DomainModel.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class Author
    {
        [Key]
        public int AuthorId { get; set; }
        public string AuthorLastName { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorMiddleName { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        public string FullName => NameHelper.GetPersonFullName(AuthorLastName, AuthorFirstName, AuthorMiddleName);
        public string ShortName => NameHelper.GetPersonShortName(AuthorLastName, AuthorFirstName, AuthorMiddleName);
    }
}
