﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class Rental
    {
        [Key]
        public int RentalId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsDeleted { get; set; }

        public int? BookId { get; set; }
        public virtual Book Book { get; set; }

        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }
    }
}
