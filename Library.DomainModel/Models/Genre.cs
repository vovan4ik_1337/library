﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class Genre
    {
        [Key]
        public int GenreId { get; set; }
        public string GenreName { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<BookGenre> BookGenres { get; set; }
    }
}
