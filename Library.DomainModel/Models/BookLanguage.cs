﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.DomainModel.Models
{
    public class BookLanguage
    {
        [Key]
        public int BookLanguageId { get; set; }
        public string BookLanguageName { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
