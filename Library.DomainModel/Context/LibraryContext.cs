﻿using Library.DomainModel.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DomainModel.Context
{
    public class LibraryContext : DbContext
    {

        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<BookGenre> BookGenres { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }
        public virtual DbSet<Rental> Rentals { get; set; }

        public Task SingleOrDefaultAsync()
        {
            throw new NotImplementedException();
        }

        public virtual DbSet<BookLanguage> BookLanguages { get; set; }
    }
}
