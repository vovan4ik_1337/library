﻿ using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.DomainModel.Migrations
{
    public partial class LittleFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_BookLanguage_BookLanguageId",
                table: "Books");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BookLanguage",
                table: "BookLanguage");

            migrationBuilder.DropColumn(
                name: "BookPublishingDate",
                table: "Books");

            migrationBuilder.RenameTable(
                name: "BookLanguage",
                newName: "BookLanguages");

            migrationBuilder.RenameColumn(
                name: "AuthirMiddleName",
                table: "Authors",
                newName: "AuthorMiddleName");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Rentals",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Clients",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Clients",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BookPublishingYear",
                table: "Books",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Books",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "BookLanguages",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_BookLanguages",
                table: "BookLanguages",
                column: "BookLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Books_BookLanguages_BookLanguageId",
                table: "Books",
                column: "BookLanguageId",
                principalTable: "BookLanguages",
                principalColumn: "BookLanguageId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_BookLanguages_BookLanguageId",
                table: "Books");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BookLanguages",
                table: "BookLanguages");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Rentals");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "BookPublishingYear",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "BookLanguages");

            migrationBuilder.RenameTable(
                name: "BookLanguages",
                newName: "BookLanguage");

            migrationBuilder.RenameColumn(
                name: "AuthorMiddleName",
                table: "Authors",
                newName: "AuthirMiddleName");

            migrationBuilder.AddColumn<DateTime>(
                name: "BookPublishingDate",
                table: "Books",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_BookLanguage",
                table: "BookLanguage",
                column: "BookLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Books_BookLanguage_BookLanguageId",
                table: "Books",
                column: "BookLanguageId",
                principalTable: "BookLanguage",
                principalColumn: "BookLanguageId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
