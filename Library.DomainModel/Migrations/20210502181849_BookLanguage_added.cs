﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.DomainModel.Migrations
{
    public partial class BookLanguage_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BookLanguageId",
                table: "Books",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BookLanguage",
                columns: table => new
                {
                    BookLanguageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookLanguageName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookLanguage", x => x.BookLanguageId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_BookLanguageId",
                table: "Books",
                column: "BookLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Books_BookLanguage_BookLanguageId",
                table: "Books",
                column: "BookLanguageId",
                principalTable: "BookLanguage",
                principalColumn: "BookLanguageId",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_BookLanguage_BookLanguageId",
                table: "Books");

            migrationBuilder.DropTable(
                name: "BookLanguage");

            migrationBuilder.DropIndex(
                name: "IX_Books_BookLanguageId",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "BookLanguageId",
                table: "Books");
        }
    }
}
