﻿using Library.DAL.Interfaces;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DAL.Repositories;
using Library.DAL.Repositories.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.DAL
{
    public static class ServiceRegistrations
    {
        public static void RegisterDAL(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IAuthorRepository, AuthorRepository>();
            serviceCollection.AddScoped<IBookLanguageRepository, BookLanguageRepository>();
            serviceCollection.AddScoped<IBookRepository, BookRepository>();
            serviceCollection.AddScoped<IClientRepository, ClientRepository>();
            serviceCollection.AddScoped<IGenreRepository, GenreRepository>();
            serviceCollection.AddScoped<IRentalRepository, RentalRepository>();
            serviceCollection.AddScoped<IBookGenreRepository, BookGenreRepository>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
