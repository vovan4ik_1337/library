﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces.IUnitOfWork
{
    public interface IUnitOfWork
    {
        IAuthorRepository AuthorRepository { get; }
        IBookLanguageRepository BookLanguageRepository { get; }
        IBookRepository BookRepository { get; }
        IClientRepository ClientRepository { get; }
        IGenreRepository GenreRepository { get; }
        IRentalRepository RentalRepository { get; }

        IBookGenreRepository BookGenreRepository { get; }

        void Commit();

        Task CommitAsync();
    }
}
