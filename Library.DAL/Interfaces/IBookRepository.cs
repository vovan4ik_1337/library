﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IBookRepository
    {
        Task<Book> GetBook(int bookId);

        Task<IEnumerable<Book>> GetBooks(string bookArticleNumber, string bookName, string bookPublishingHouse,
                                            string bookSeriesName, string bookPublishingYear, int? bookAgeLimit, 
                                            int? authorId, int? bookLanguageId, int? genreId, int? start, int? end);

        Task AddBook(Book book);

        Task UpdateBook(int bookId, Book book);

        Task DeleteBook(int bookId);
    }
}
