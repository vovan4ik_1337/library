﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IGenreRepository
    {
        Task<Genre> GetGenre(int genreId);

        Task<IEnumerable<Genre>> GetGenres();

        Task AddGenre(Genre genre);

        Task UpdateGenre(int genreId, Genre genre);

        Task DeleteGenre(int genreId);
    }
}
