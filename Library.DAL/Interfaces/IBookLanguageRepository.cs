﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IBookLanguageRepository
    {
        Task<BookLanguage> GetBookLanguage(int bookLanguageId);

        Task<IEnumerable<BookLanguage>> GetBookLanguages();

        Task AddBookLanguage(BookLanguage bookLanguage);

        Task UpdateBookLanguage(int bookLanguageId, BookLanguage bookLanguage);

        Task DeleteBookLanguage(int bookLanguageId);
    }
}
