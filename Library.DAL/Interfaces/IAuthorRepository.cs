﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IAuthorRepository
    {
        Task<Author> GetAuthor(int authorId);

        Task<IEnumerable<Author>> GetAuthors(string authorLastName, string authorFirstName, string authorMiddleName,
            int? start, int? end);

        Task AddAuthor(Author author);

        Task UpdateAuthor(int authorId, Author author);

        Task DeleteAuthor(int authorId);
    }
}
