﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IRentalRepository
    {
        Task<Rental> GetRental(int rentalId);

        Task<IEnumerable<Rental>> GetRentals(DateTime? startDate, DateTime? endDate, int? bookId, int? clientId);

        Task AddRental(Rental rental);

        Task UpdateRental(int rentalId, Rental rental);

        Task DeleteRental(int rentalId);
    }
}
