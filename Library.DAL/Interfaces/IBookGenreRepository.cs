﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IBookGenreRepository
    {
        Task<BookGenre> GetBookGenre(int bookGenreId);

        Task<IEnumerable<BookGenre>> GetBookGenres(int? bookId, int? genreId);

        Task AddBookGenre(BookGenre bookGenre);

        Task UpdateGenre(int bookGenreId, BookGenre bookGenre);

        Task DeleteBookGenre(int bookGenreId);
    }
}
