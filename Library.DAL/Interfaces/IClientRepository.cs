﻿using Library.DomainModel.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IClientRepository
    {
        Task<Client> GetClient(int clientId);

        Task<IEnumerable<Client>> GetClients(string clientLastName, string clientFirstName, string clientMiddleNamer);

        Task AddClient(Client client);

        Task UpdateClient(int clientId, Client client);

        Task DeleteClient(int clientId);
    }
}
