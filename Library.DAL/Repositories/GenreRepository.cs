﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private readonly LibraryContext _context;

        public GenreRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddGenre(Genre genre)
        {
            if (genre != null)
                await _context.Genres.AddAsync(genre);
        }

        public async Task DeleteGenre(int genreId)
        {
            var genre = await _context.Genres.SingleOrDefaultAsync(x => x.GenreId == genreId);

            if (genre != null)
            {
                _context.Genres.Remove(genre);
            }
        }

        public async Task<Genre> GetGenre(int genreId)
        {
            return await _context.Genres.SingleOrDefaultAsync(x => x.GenreId == genreId);
        }

        public async Task<IEnumerable<Genre>> GetGenres()
        {
            return await _context.Genres.ToListAsync();
        }

        public async Task UpdateGenre(int genreId, Genre genre)
        {
            var oldGenre = await _context.Genres.SingleOrDefaultAsync(x => x.GenreId == genreId);

            if (oldGenre != null && genre != null)
            {
                if (!string.IsNullOrWhiteSpace(genre.GenreName))
                    oldGenre.GenreName = genre.GenreName;
            }
        }
    }
}
