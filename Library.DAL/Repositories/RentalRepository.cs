﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class RentalRepository : IRentalRepository
    {
        private readonly LibraryContext _context;

        public RentalRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddRental(Rental rental)
        {
            if (rental != null)
                await _context.Rentals.AddAsync(rental);
        }

        public async Task DeleteRental(int rentalId)
        {
            var rental = await _context.Rentals.SingleOrDefaultAsync(x => x.RentalId == rentalId);

            if (rental != null)
            {
                _context.Rentals.Remove(rental);
            }
        }

        public async Task<Rental> GetRental(int rentalId)
        {
            return await _context.Rentals.Include(x => x.Book).Include(x => x.Client).SingleOrDefaultAsync(x => x.RentalId == rentalId);
        }

        public async Task<IEnumerable<Rental>> GetRentals(DateTime? startDate, DateTime? endDate, int? bookId, int? clientId)
        {
            var rentals = await _context.Rentals.Include(x => x.Book).Include(x => x.Client).Where(x => !x.IsDeleted &&
            (startDate == null || (x.StartDate.Value.Date >= startDate.Value.Date)) &&
            (endDate == null || (x.EndDate.Value.Date <= endDate.Value.Date)) &&
            (bookId == null || (x.BookId == bookId)) &&
            (clientId == null || (x.ClientId == clientId))).ToListAsync();

            return rentals;
        }

        public async Task UpdateRental(int rentalId, Rental rental)
        {
            var oldRental = await _context.Rentals.SingleOrDefaultAsync(x => x.RentalId == rentalId);

            if (oldRental != null && rental != null)
            {
                if (rental.StartDate != null)
                    oldRental.StartDate = rental.StartDate.Value.Date;

                if (rental.EndDate != null)
                    oldRental.EndDate = rental.EndDate.Value.Date;

                if (rental.BookId != null)
                    oldRental.BookId = rental.BookId;

                if (rental.ClientId != null)
                    oldRental.ClientId = rental.ClientId;
            }
        }
    }
}
