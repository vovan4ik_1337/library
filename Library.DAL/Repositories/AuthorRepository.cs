﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly LibraryContext _context;

        public AuthorRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddAuthor(Author author)
        {
            if (author != null)
                await _context.Authors.AddAsync(author);
        }

        public async Task DeleteAuthor(int authorId)
        {
            var author = await _context.Authors.SingleOrDefaultAsync(x => x.AuthorId == authorId);

            if (author != null)
            {
                _context.Authors.Remove(author);
            }
        }

        public async Task<Author> GetAuthor(int authorId)
        {
            return await _context.Authors.SingleOrDefaultAsync(x => x.AuthorId == authorId);
        }

        public async Task<IEnumerable<Author>> GetAuthors(string authorLastName, string authorFirstName, string authorMiddleName, int? start, int? end)
        {

            var authors = await _context.Authors.Where(x => !x.IsDeleted &&
            (string.IsNullOrWhiteSpace(authorLastName) || x.AuthorLastName.Contains(authorLastName)) &&
            (string.IsNullOrWhiteSpace(authorFirstName) || x.AuthorFirstName.Contains(authorFirstName)) &&
            (string.IsNullOrWhiteSpace(authorMiddleName) || x.AuthorMiddleName.Contains(authorMiddleName))).ToListAsync();

            if (start != null && end != null)
            {
                var skip = start;
                var take = end - start;

                authors = authors.Skip(skip ?? 0).Take(take ?? 0).ToList();
            }

            return authors;
        }

        public async Task UpdateAuthor(int authorId, Author author)
        {
            var oldAuthor = await _context.Authors.SingleOrDefaultAsync(x => x.AuthorId == authorId);

            if (author != null && oldAuthor != null)
            {
                if (!string.IsNullOrWhiteSpace(author.AuthorLastName))
                    oldAuthor.AuthorLastName = author.AuthorLastName;

                if (!string.IsNullOrWhiteSpace(author.AuthorFirstName))
                    oldAuthor.AuthorFirstName = author.AuthorFirstName;

                if (!string.IsNullOrWhiteSpace(author.AuthorMiddleName))
                    oldAuthor.AuthorMiddleName = author.AuthorMiddleName;
            }
        }
    }
}
