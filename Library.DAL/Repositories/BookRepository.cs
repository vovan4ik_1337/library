﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext _context;

        public BookRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddBook(Book book)
        {
            if (book != null)
                await _context.Books.AddAsync(book);
        }

        public async Task DeleteBook(int bookId)
        {
            var book = await _context.Books.SingleOrDefaultAsync(x => x.BookId == bookId);

            if (book != null)
            {
                _context.Books.Remove(book);
            }
        }

        public async Task<Book> GetBook(int bookId)
        {
            return await _context.Books.Include(x => x.BookLanguage).Include(x => x.Author).SingleOrDefaultAsync(x => x.BookId == bookId);
        }

        public async Task<IEnumerable<Book>> GetBooks(string bookArticleNumber, string bookName, string bookPublishingHouse, 
            string bookSeriesName, string bookPublishingYear, int? bookAgeLimit, int? authorId, int? bookLanguageId, 
            int? bookGenreId ,int? start, int? end)
        {
            var books = await _context.Books.Include(x => x.BookLanguage).Include(x => x.Author).
                Where(x => !x.IsDeleted && 
            (string.IsNullOrWhiteSpace(bookArticleNumber) || (x.BookArticleNumber == bookArticleNumber)) &&
            (string.IsNullOrWhiteSpace(bookName) || (x.BookName.Contains(bookName))) &&
            (string.IsNullOrWhiteSpace(bookPublishingHouse) || (x.BookPublishingHouse.Contains(bookPublishingHouse))) &&
            (string.IsNullOrWhiteSpace(bookSeriesName) || (x.BookSeriesName.Contains(bookSeriesName))) &&
            (string.IsNullOrWhiteSpace(bookPublishingYear) || (x.BookPublishingYear == bookPublishingYear)) &&
            (bookAgeLimit == null || (x.BookAgeLimit == bookAgeLimit)) &&
            (authorId == null || (x.AuthorId == authorId)) &&
            (bookLanguageId == null || (x.BookLanguageId  == bookLanguageId))
            ).ToListAsync();

            if (start != null && end != null)
            {
                var skip = start;
                var take = end - start;

                books = books.Skip(skip ?? 0).Take(take ?? 0).ToList();
            }

            return books;
        }

        public async Task UpdateBook(int bookId, Book book)
        {
            var oldBook = await _context.Books.SingleOrDefaultAsync(x => x.BookId == bookId);

            if (oldBook != null && book != null)
            {
                if (!string.IsNullOrWhiteSpace(book.BookArticleNumber))
                    oldBook.BookArticleNumber = book.BookArticleNumber;

                if (!string.IsNullOrWhiteSpace(book.BookName))
                    oldBook.BookName = book.BookName;

                if (!string.IsNullOrWhiteSpace(book.BookPublishingHouse))
                    oldBook.BookPublishingHouse = book.BookPublishingHouse;

                if (!string.IsNullOrWhiteSpace(book.BookSeriesName))
                    oldBook.BookSeriesName = book.BookSeriesName;

                if (!string.IsNullOrWhiteSpace(book.BookPublishingYear))
                    oldBook.BookPublishingYear = book.BookPublishingYear;

                if (book.BookPrice != null)
                    oldBook.BookPrice = book.BookPrice;

                if (book.BookAgeLimit != null)
                    oldBook.BookAgeLimit = book.BookAgeLimit;

                if (book.AuthorId != null)
                    oldBook.AuthorId = book.AuthorId;

                if (book.BookLanguageId != null)
                    oldBook.BookLanguageId = book.BookLanguageId;
            }
        }
    }
}
