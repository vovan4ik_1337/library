﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class BookLanguageRepository : IBookLanguageRepository
    {
        private readonly LibraryContext _context;

        public BookLanguageRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddBookLanguage(BookLanguage bookLanguage)
        {
            if (bookLanguage != null)
                await _context.BookLanguages.AddAsync(bookLanguage);
        }

        public async Task DeleteBookLanguage(int bookLanguageId)
        {
            var bookLanguage = await _context.BookLanguages.SingleOrDefaultAsync(x => x.BookLanguageId == bookLanguageId);

            if (bookLanguage != null)
            {
                _context.BookLanguages.Remove(bookLanguage);
            }
        }

        public async Task<BookLanguage> GetBookLanguage(int bookLanguageId)
        {
            return await _context.BookLanguages.SingleOrDefaultAsync(x => x.BookLanguageId == bookLanguageId);
        }

        public async Task<IEnumerable<BookLanguage>> GetBookLanguages()
        {
            return await _context.BookLanguages.ToListAsync();
        }

        public async Task UpdateBookLanguage(int bookLanguageId, BookLanguage bookLanguage)
        {
            var oldBookLanguage = await _context.BookLanguages.SingleOrDefaultAsync(x => x.BookLanguageId == bookLanguageId);

            if (oldBookLanguage != null || bookLanguage != null)
            {
                if (!string.IsNullOrWhiteSpace(bookLanguage.BookLanguageName))
                    oldBookLanguage.BookLanguageName = bookLanguage.BookLanguageName;
            }
        }
    }
}
