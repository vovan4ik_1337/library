﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class BookGenreRepository : IBookGenreRepository
    {
        private readonly LibraryContext _context;

        public BookGenreRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddBookGenre(BookGenre bookGenre)
        {
            if (bookGenre != null)
                await _context.BookGenres.AddAsync(bookGenre);
        }

        public async Task DeleteBookGenre(int bookGenreId)
        {
            var bookGenre = await _context.BookGenres.SingleOrDefaultAsync(x => x.BookGenreId == bookGenreId);

            if (bookGenre != null)
            {
                _context.BookGenres.Remove(bookGenre);
            }
        }

        public async Task<BookGenre> GetBookGenre(int bookGenreId)
        {
            return await _context.BookGenres.Include(x => x.Book).Include(x => x.Genre).SingleOrDefaultAsync(x => x.BookGenreId == bookGenreId);
        }

        public async Task<IEnumerable<BookGenre>> GetBookGenres(int? bookId, int? genreId)
        {
            return await _context.BookGenres.Include(x => x.Book)
                .Include(x => x.Genre).Where(x => !x.IsDeleted &&
            (bookId == null || (x.BookId == bookId)) &&
            (genreId == null || (x.GenreId == genreId))).ToListAsync();
        }

        public async Task UpdateGenre(int bookGenreId, BookGenre bookGenre)
        {
            var oldBookGenre = await _context.BookGenres.SingleOrDefaultAsync(x => x.BookGenreId == bookGenreId);

            if (oldBookGenre != null && bookGenre != null)
            {
                if (bookGenre.BookId != null)
                    oldBookGenre.BookId = bookGenre.BookId;

                if (bookGenre.GenreId != null)
                    oldBookGenre.GenreId = bookGenre.GenreId;
            }
        }
    }
}
