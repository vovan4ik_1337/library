﻿using Library.DAL.Interfaces;
using Library.DomainModel.Context;
using Library.DomainModel.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly LibraryContext _context;

        public ClientRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task AddClient(Client client)
        {
            if (client != null)
                await _context.Clients.AddAsync(client);           
        }

        public async Task DeleteClient(int clientId)
        {
            var client = await _context.Clients.SingleOrDefaultAsync(x => x.ClientId == clientId);

            if (client != null)
            {
                _context.Clients.Remove(client);
            }
        }

        public async Task<Client> GetClient(int clientId)
        {
            return await _context.Clients.SingleOrDefaultAsync(x => x.ClientId == clientId);
        }

        public async Task<IEnumerable<Client>> GetClients(string clientLastName, string clientFirstName, string clientMiddleName)
        {
            var clients = await _context.Clients.Where(x => !x.IsDeleted &
            (string.IsNullOrWhiteSpace(clientLastName) || (x.ClientLastName.Contains(clientLastName))) &&
            (string.IsNullOrWhiteSpace(clientFirstName) || (x.ClientFirstName.Contains(clientFirstName))) &&
            (string.IsNullOrWhiteSpace(clientMiddleName) || (x.ClientMiddleName.Contains(clientMiddleName)))).ToListAsync();

            return clients;
        }

        public async Task UpdateClient(int clientId, Client client)
        {
            var oldClient = await _context.Clients.SingleOrDefaultAsync(x => x.ClientId == clientId);

            if (oldClient != null && client != null)
            {
                if (!string.IsNullOrWhiteSpace(client.ClientLastName))
                    oldClient.ClientLastName = client.ClientLastName;

                if (!string.IsNullOrWhiteSpace(client.ClientFirstName))
                    oldClient.ClientFirstName = client.ClientFirstName;

                if (!string.IsNullOrWhiteSpace(client.ClientMiddleName))
                    oldClient.ClientMiddleName = client.ClientMiddleName;

                if (client.Age != null)
                    oldClient.Age = client.Age;

                if (client.PhoneNumber != null)
                    oldClient.PhoneNumber = client.PhoneNumber;                
            }    
        }
    }
}
