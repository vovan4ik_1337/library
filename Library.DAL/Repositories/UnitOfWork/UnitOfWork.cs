﻿using Library.DAL.Interfaces;
using Library.DAL.Interfaces.IUnitOfWork;
using Library.DomainModel.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext _context;

        public UnitOfWork(LibraryContext context,
            IAuthorRepository authorRepository,
            IBookLanguageRepository bookLanguageRepository,
            IBookRepository bookRepository,
            IClientRepository clientRepository,
            IGenreRepository genreRepository,
            IRentalRepository rentalRepository,
            IBookGenreRepository bookGenreRepository)
        {
            _context = context;

            AuthorRepository = authorRepository;
            BookLanguageRepository = bookLanguageRepository;
            BookRepository = bookRepository;
            ClientRepository = clientRepository;
            GenreRepository = genreRepository;
            RentalRepository = rentalRepository;
            BookGenreRepository = bookGenreRepository;
        }

        public IAuthorRepository AuthorRepository { get; }
        public IBookLanguageRepository BookLanguageRepository { get; }
        public IBookRepository BookRepository { get; }
        public IClientRepository ClientRepository { get; }
        public IGenreRepository GenreRepository { get; }
        public IRentalRepository RentalRepository { get; }
        public IBookGenreRepository BookGenreRepository { get; }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
