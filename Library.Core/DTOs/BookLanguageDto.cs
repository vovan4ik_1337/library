﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.DTOs
{
    public class BookLanguageDto
    {
        public int BookLanguageId { get; set; }
        public string BookLanguageName { get; set; }
    }
}
