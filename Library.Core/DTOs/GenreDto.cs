﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.DTOs
{
    public class GenreDto
    {
        public int GenreId { get; set; }
        public string GenreName { get; set; }
    }
}
