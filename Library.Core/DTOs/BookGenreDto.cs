﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.DTOs
{
    public class BookGenreDto
    {
        public int BookGenreId { get; set; }

        public int? BookId { get; set; }
        public string Book { get; set; }

        public int? GenreId { get; set; }
        public string Genre { get; set; }
    }
}
