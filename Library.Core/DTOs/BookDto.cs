﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.DTOs
{
    public class BookDto
    {
        public int BookId { get; set; }
        public string BookArticleNumber { get; set; }
        public string BookName { get; set; }
        public string BookPublishingHouse { get; set; }
        public string BookSeriesName { get; set; }
        public string BookPublishingYear { get; set; }
        public decimal? BookPrice { get; set; }
        public int? BookAgeLimit { get; set; }

        public int? AuthorId { get; set; }
        public string Author { get; set; }

        public int? BookLanguageId { get; set; }
        public string BookLanguage { get; set; }
    }
}
