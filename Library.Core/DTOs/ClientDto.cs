﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.DTOs
{
    public class ClientDto
    {
        public int ClientId { get; set; }
        public string ClientLastName { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientMiddleName { get; set; }
        public int? Age { get; set; }
        public string PhoneNumber { get; set; }
    }
}
