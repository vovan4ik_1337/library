﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class BookRequest
    {
        public string BookArticleNumber { get; set; }
        public string BookName { get; set; }
        public string BookPublishingHouse { get; set; }
        public string BookSeriesName { get; set; }
        public string BookPublishingYear { get; set; }
        public decimal? BookPrice { get; set; }
        public int? BookAgeLimit { get; set; }

        public int? AuthorId { get; set; }

        public int? BookLanguageId { get; set; }
    }
}
