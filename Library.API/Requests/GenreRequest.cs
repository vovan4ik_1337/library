﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class GenreRequest
    {
        public string GenreName { get; set; }
    }
}
