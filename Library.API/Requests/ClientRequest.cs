﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class ClientRequest
    {
        public string ClientLastName { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientMiddleName { get; set; }
        public int? Age { get; set; }
        public string PhoneNumber { get; set; }
    }
}
