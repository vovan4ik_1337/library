﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class AuthorRequest
    {
        public string AuthorLastName { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorMiddleName { get; set; }
    }
}
