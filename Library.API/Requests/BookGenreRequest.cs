﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class BookGenreRequest
    {
        public int? BookId { get; set; }
        public int? GenreId { get; set; }
    }
}
