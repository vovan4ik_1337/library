﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Requests
{
    public class RentalRequest
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? BookId { get; set; }

        public int? ClientId { get; set; }
    }
}
