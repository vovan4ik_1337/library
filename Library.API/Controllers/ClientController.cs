﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public ClientController(IClientService clientService, IMapper mapper)
        {
            _clientService = clientService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<ClientViewModel>> GetClient(int clientId)
        {
            var clientDto = await _clientService.GetClient(clientId);

            var clientView = _mapper.Map<ClientViewModel>(clientDto);

            return Ok(clientView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<ClientViewModel>>> GetClients(string clientLastName, string clientFirstName,
            string clientMiddleNamer)
        {
            var clientsDtos = await _clientService.GetClients(clientLastName, clientFirstName, clientMiddleNamer);

            var clientsViews = _mapper.Map<IEnumerable<ClientViewModel>>(clientsDtos);

            return Ok(clientsViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddClient([FromBody] ClientRequest request)
        {
            var clientDto = new ClientDto
            {
                ClientLastName = request.ClientLastName,
                ClientFirstName = request.ClientFirstName,
                ClientMiddleName = request.ClientMiddleName,
                Age = request.Age,
                PhoneNumber = request.PhoneNumber
            };

            try
            {
                await _clientService.AddClient(clientDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при добавлении читателя");
            }
        }

        [HttpPut("[action]/{clientId}")]
        public async Task<ActionResult> UpdateClient(int clientId, [FromBody] ClientRequest request)
        {
            var clientDto = new ClientDto
            {
                ClientLastName = request.ClientLastName,
                ClientFirstName = request.ClientFirstName,
                ClientMiddleName = request.ClientMiddleName,
                Age = request.Age,
                PhoneNumber = request.PhoneNumber
            };

            try
            {
                await _clientService.UpdateClient(clientId, clientDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении читателя");
            }
        }

        [HttpDelete("[action]/{clientId}")]
        public async Task<ActionResult> DeleteClient(int clientId)
        {
            try
            {
                await _clientService.DeleteClient(clientId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении читателя");
            }
        }
    }
}
