﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookGenreController : ControllerBase
    {
        private readonly IBookGenreService _bookGenreService;
        private readonly IMapper _mapper;

        public BookGenreController(IBookGenreService bookGenreService, IMapper mapper)
        {
            _bookGenreService = bookGenreService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<BookGenreViewModel>> GetBookGenre(int bookGenreId)
        {
            var bookGenreDto = await _bookGenreService.GetBookGenre(bookGenreId);

            var bookGenreView = _mapper.Map<BookGenreViewModel>(bookGenreDto);

            return Ok(bookGenreView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<BookGenreViewModel>>> GetBookGenres(int? bookId, int? genreId)
        {
            var bookGenresDtos = await _bookGenreService.GetBookGenres(bookId, genreId);

            var bookGenresViews = _mapper.Map<IEnumerable<BookGenreViewModel>>(bookGenresDtos);

            return Ok(bookGenresViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddBookGenre([FromBody] BookGenreRequest request)
        {
            var bookGenreDto = new BookGenreDto
            { 
                BookId = request.BookId,
                GenreId = request.GenreId
            };

            try
            {
                await _bookGenreService.AddBookGenre(bookGenreDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при создании записи BookGenre");
            }
        }

        [HttpPut("[action]/{bookGenreId}")]
        public async Task<ActionResult> UpdateBookGenre(int bookGenreId, [FromBody] BookGenreRequest request)
        {
            var bookGenreDto = new BookGenreDto
            {
                BookId = request.BookId,
                GenreId = request.GenreId
            };

            try
            {
                await _bookGenreService.UpdateGenre(bookGenreId, bookGenreDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении записи BookGenre");
            }
        }

        [HttpDelete("[action]/{bookGenreId}")]
        public async Task<ActionResult> DeleteBookGenre(int bookGenreId)
        {
            try
            {
                await _bookGenreService.DeleteBookGenre(bookGenreId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении записи BookGenre");
            }
        }
    }
}
