﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;

        public BookController(IBookService bookService, IMapper mapper)
        {
            _bookService = bookService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<BookViewModel>> GetBook(int bookId)
        {
            var bookDto = await _bookService.GetBook(bookId);

            var bookView = _mapper.Map<BookViewModel>(bookDto);

            return Ok(bookView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<BookViewModel>>> GetBooks(string bookArticleNumber, string bookName, string bookPublishingHouse,
                                            string bookSeriesName, string bookPublishingYear, int? bookAgeLimit,
                                            int? authorId, int? bookLanguageId, int? genreId, int? start, int? end)
        {
            var booksDtos = await _bookService.GetBooks(bookArticleNumber, bookName, bookPublishingHouse,
                                            bookSeriesName, bookPublishingYear, bookAgeLimit,
                                            authorId, bookLanguageId, genreId, start, end);

            var booksViews = _mapper.Map<IEnumerable<BookViewModel>>(booksDtos);

            return Ok(booksViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddBook([FromBody] BookRequest request)
        {
            var bookDto = new BookDto
            { 
                BookArticleNumber = request.BookArticleNumber,
                BookName = request.BookName,
                BookPublishingHouse = request.BookPublishingHouse,
                BookSeriesName = request.BookSeriesName,
                BookPublishingYear = request.BookPublishingYear,
                BookPrice = request.BookPrice,
                BookAgeLimit = request.BookAgeLimit,
                AuthorId = request.AuthorId,
                BookLanguageId = request.BookLanguageId
            };


            try
            {
                await _bookService.AddBook(bookDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при добавлении книги");
            }
        }

        [HttpPut("[action]/{bookId}")]
        public async Task<ActionResult> UpdateBook(int bookId, [FromBody] BookRequest request)
        {
            var bookDto = new BookDto
            {
                BookArticleNumber = request.BookArticleNumber,
                BookName = request.BookName,
                BookPublishingHouse = request.BookPublishingHouse,
                BookSeriesName = request.BookSeriesName,
                BookPublishingYear = request.BookPublishingYear,
                BookPrice = request.BookPrice,
                BookAgeLimit = request.BookAgeLimit,
                AuthorId = request.AuthorId,
                BookLanguageId = request.BookLanguageId
            };

            try
            {
                await _bookService.UpdateBook(bookId ,bookDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении книги");
            }
        }

        [HttpDelete("[action]/{bookId}")]
        public async Task<ActionResult> DeleteBook(int bookId)
        {
            try
            {
                await _bookService.DeleteBook(bookId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении книги");
            }
        }
    }
}
