﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookLanguageController : ControllerBase
    {
        private readonly IBookLanguageService _bookLanguageService;
        private readonly IMapper _mapper;

        public BookLanguageController(IBookLanguageService bookLanguageService, IMapper mapper)
        {
            _bookLanguageService = bookLanguageService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<BookLanguageViewModel>> GetBookLanguage(int bookLanguageId)
        {
            var bookLanguageDto = await _bookLanguageService.GetBookLanguage(bookLanguageId);

            var bookLanguageView = _mapper.Map<BookLanguageViewModel>(bookLanguageDto);

            return Ok(bookLanguageView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<BookLanguageViewModel>>> GetBookLanguages()
        {
            var bookLanguagesDtos = await _bookLanguageService.GetBookLanguages();

            var bookLanguagesViews = _mapper.Map<IEnumerable<BookLanguageViewModel>>(bookLanguagesDtos);

            return Ok(bookLanguagesViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddBookLanguage([FromBody] BookLanguageRequest request)
        {
            var bookLanguageDto = new BookLanguageDto
            { 
                BookLanguageName = request.BookLanguageName
            };

            try
            {
                await _bookLanguageService.AddBookLanguage(bookLanguageDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при добавлении языка");
            }          
        }

        [HttpPut("[action]/{bookLanguageId}")]
        public async Task<ActionResult> UpdateBookLanguage(int bookLanguageId, [FromBody] BookLanguageRequest request)
        {
            var bookLanguageDto = new BookLanguageDto
            {
                BookLanguageName = request.BookLanguageName
            };

            try
            {
                await _bookLanguageService.UpdateBookLanguage(bookLanguageId ,bookLanguageDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении языка");
            }
        }

        [HttpDelete("[action]/{bookLanguageId}")]
        public async Task<ActionResult> DeleteBookLanguage(int bookLanguageId)
        {
            try
            {
                await _bookLanguageService.DeleteBookLanguage(bookLanguageId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении языка");
            }
        }
    }
}
