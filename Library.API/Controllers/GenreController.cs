﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;

        public GenreController(IGenreService genreService, IMapper mapper)
        {
            _genreService = genreService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<GenreViewModel>> GetGenre(int genreId)
        {
            var genreDto = await _genreService.GetGenre(genreId);

            var genreView = _mapper.Map<GenreViewModel>(genreDto);

            return Ok(genreView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<GenreViewModel>>> GetGenres()
        {
            var genresDtos = await _genreService.GetGenres();

            var genresViews = _mapper.Map<IEnumerable<GenreViewModel>>(genresDtos);

            return Ok(genresViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddGenre([FromBody] GenreRequest request)
        {
            var genreDto = new GenreDto
            {
                GenreName = request.GenreName
            };

            try
            {
                await _genreService.AddGenre(genreDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при добавлении жанра");
            }
        }

        [HttpPut("[action]/{genreId}")]
        public async Task<ActionResult> UpdateGenre(int genreId, [FromBody] GenreRequest request)
        {
            var genreDto = new GenreDto
            {
                GenreName = request.GenreName
            };

            try
            {
                await _genreService.UpdateGenre(genreId ,genreDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении жанра");
            }
        }

        [HttpDelete("[action]/{genreId}")]
        public async Task<ActionResult> DeleteGenre(int genreId)
        {
            try
            {
                await _genreService.DeleteGenre(genreId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении жанра");
            }
        }
    }
}
