﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorService authorService, IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<AuthorViewModel>> GetAuthor(int authorId)
        {
            var authorDto = await _authorService.GetAuthor(authorId);

            var authorView = _mapper.Map<AuthorViewModel>(authorDto);

            return Ok(authorView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<AuthorViewModel>>> GetAuthors(string authorLastName, string authorFirstName, string authorMiddleName,
            int? start, int? end)
        {
            var authorsDtos = await _authorService.GetAuthors(authorLastName, authorFirstName, authorMiddleName, start, end);

            var auhtorsViews = _mapper.Map<IEnumerable<AuthorViewModel>>(authorsDtos);

            return Ok(auhtorsViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddAuthor([FromBody] AuthorRequest request)
        {
            var authorDto = new AuthorDto
            {
                AuthorLastName = request.AuthorLastName,
                AuthorFirstName = request.AuthorFirstName,
                AuthorMiddleName = request.AuthorMiddleName
            };

            try
            {
                await _authorService.AddAuthor(authorDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при добавлении автора");
            }           
        }

        [HttpPut("[action]/{authorId}")]
        public async Task<ActionResult> UpdateAuthor(int authorId, [FromBody] AuthorRequest request)
        {
            var authorDto = new AuthorDto
            {
                AuthorLastName = request.AuthorLastName,
                AuthorFirstName = request.AuthorFirstName,
                AuthorMiddleName = request.AuthorMiddleName
            };

            try
            {
                await _authorService.UpdateAuthor(authorId ,authorDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении автора");
            }
        }

        [HttpDelete("[action]/{authorId}")]
        public async Task<ActionResult> DeleteAuthor(int authorId)
        {
            try
            {
                await _authorService.DeleteAuthor(authorId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении автора");
            }


        }
    }
}
