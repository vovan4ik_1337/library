﻿using AutoMapper;
using Library.API.Requests;
using Library.API.ViewModels;
using Library.BLL.Interfaces;
using Library.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RentalController : ControllerBase
    {
        private readonly IRentalService _rentalService;
        private readonly IMapper _mapper;

        public RentalController(IRentalService rentalService, IMapper mapper)
        {
            _rentalService = rentalService;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<RentalViewModel>> GetRental(int rentalId)
        {
            var rentalDto = await _rentalService.GetRental(rentalId);

            var rentalView = _mapper.Map<RentalViewModel>(rentalDto);

            return Ok(rentalView);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<RentalViewModel>>> GetRentals(DateTime? startDate, DateTime? endDate, int? bookId, int? clientId)
        {
            var rentalsDtos = await _rentalService.GetRentals(startDate, endDate, bookId, clientId);

            var rentalViews = _mapper.Map<IEnumerable<RentalViewModel>>(rentalsDtos);

            return Ok(rentalViews);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> AddRental([FromBody] RentalRequest request)
        {
            var rentalDto = new RentalDto
            { 
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                BookId = request.BookId,
                ClientId = request.ClientId
            };

            try
            {
                await _rentalService.AddRental(rentalDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при создании аренды");
            }
        }

        [HttpPut("[action]/{rentalId}")]
        public async Task<ActionResult> UpdateRental(int rentalId, [FromBody] RentalRequest request)
        {
            var rentalDto = new RentalDto
            {
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                BookId = request.BookId,
                ClientId = request.ClientId
            };

            try
            {
                await _rentalService.UpdateRental(rentalId, rentalDto);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при изменении аренды");
            }
        }

        [HttpDelete("[action]/{rentalId}")]
        public async Task<ActionResult> DeleteRental(int rentalId)
        {
            try
            {
                await _rentalService.DeleteRental(rentalId);

                return Ok();
            }
            catch
            {
                throw new Exception("Ошибка при удалении аренды");
            }
        }
    }
}
