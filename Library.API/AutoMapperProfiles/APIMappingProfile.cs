﻿using AutoMapper;
using Library.API.ViewModels;
using Library.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.AutoMapperProfiles
{
    public class APIMappingProfile : Profile
    {
        public APIMappingProfile()
        {
            CreateMap<AuthorDto, AuthorViewModel>();
            CreateMap<BookLanguageDto, BookLanguageViewModel>();
            CreateMap<BookDto, BookViewModel>();
            CreateMap<ClientDto, ClientViewModel>();
            CreateMap<GenreDto, GenreViewModel>();
            CreateMap<RentalDto, RentalViewModel>();
            CreateMap<BookGenreDto, BookGenreViewModel>();
        }
    }
}
