﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Library.API
{
    public static class ServiceRegistrations
    {
        public static void RegisterAPI(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.AddAutoMapper(Assembly.GetCallingAssembly());

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificMethods",
                    option =>
                    {
                        option.WithOrigins(configuration["AllowedOrigins"])
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });


        }
    }
}
