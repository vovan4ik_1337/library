﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.ViewModels
{
    public class RentalViewModel
    {
        public int RentalId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? BookId { get; set; }
        public string Book { get; set; }

        public int? ClientId { get; set; }
        public string Client { get; set; }
    }
}
