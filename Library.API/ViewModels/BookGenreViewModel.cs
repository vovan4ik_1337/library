﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.ViewModels
{
    public class BookGenreViewModel
    {
        public int BookGenreId { get; set; }

        public int? BookId { get; set; }
        public string Book { get; set; }

        public int? GenreId { get; set; }
        public string Genre { get; set; }
    }
}
