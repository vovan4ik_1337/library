﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.ViewModels
{
    public class GenreViewModel
    {
        public int GenreId { get; set; }
        public string GenreName { get; set; }
    }
}
