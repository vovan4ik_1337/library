﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.ViewModels
{
    public class BookLanguageViewModel
    {
        public int BookLanguageId { get; set; }
        public string BookLanguageName { get; set; }
    }
}
